// vigenere

#include <stdio.h>
#include <cs50.h>
#include <string.h>
#include <ctype.h>

int main(int argc, string argv[]) {
    // verify two input arguments
    if (!(argc == 2)) {
        printf("Usage: ./vigenere k\n");
        return 1;
    }
    // verify that the second argument is purely alphabetical
    for (int i = 0, length = strlen(argv[1]); i < length; i++) {
        if (!(isalpha(argv[1][i]))) {
            printf("Usage: ./vigenere k\n");
            return 1;
        }
    }

    string cyphertext = argv[1];

    printf("plaintext:  ");
    string plaintext = get_string();

    printf("ciphertext: ");

    for (int rotation, letter_case, index_plaintext = 0, index_key = 0, key_length = strlen(cyphertext); index_plaintext < strlen(plaintext); index_plaintext++) {
        // non-alpha characters should be passed through unaltered
        if (!(isalpha(plaintext[index_plaintext]))) {
            printf("%c", plaintext[index_plaintext]);
        } else {
            if (isupper(plaintext[index_plaintext]))
                letter_case = 65;
            else
                letter_case = 97;

            rotation = cyphertext[index_key % key_length] - letter_case;
            printf("%c", ((plaintext[index_plaintext] + rotation - letter_case) % 26) + letter_case);

            // increment the key
            index_key++;
        }
    }

    printf("\n");
    return 0;
}